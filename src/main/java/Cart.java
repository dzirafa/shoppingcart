import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dzirafa on 19.02.2018
 */
public class Cart {
    private List<ProductWithQty> listOfProductsWithQty;
    private BigDecimal totalAmount;
    private int quantityOfProducts;

    public Cart() {
        this.listOfProductsWithQty = new ArrayList<>();
        this.totalAmount = BigDecimal.ZERO;
        this.quantityOfProducts = 0;
    }

    public List<ProductWithQty> getListOfProductsWithQty() {
        return listOfProductsWithQty;
    }

    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    public int getQuantityOfProducts() {
        return quantityOfProducts;
    }

    /**
     * Finds the product (with a name productName) on the list of Products from AllProducts class.
     * It is only one product with a name productName on the list (productName unique).
     * @param productName - name of the product
     * @return product with a name productName (if does not exist, the product is null)
     */
    public Product getProductByName(String productName) {
        Product foundProduct = null;
        List<Product> products = new AllProducts().getAllAvailableProducts();
        for (Product product : products) {
            if (productName.equals(product.getName())) {
                foundProduct = product;
            }
        }
        return foundProduct;
    }

    /**
     * Finds the product (with a name productName) combined with quantity on the list of Products from the cart.
     * It is only one product with a name productName on the list (productName unique).
     * @param productName - name of the product
     * @return product with a name productName (if does not exist, the product is null) combined with quantity
     */
    public ProductWithQty getProductWithQtyByName(String productName) {
        ProductWithQty foundProductWithQty = null;
        for (ProductWithQty productWithQty : listOfProductsWithQty) {
            if (productName.equals(productWithQty.getProduct().getName())) {
                foundProductWithQty = productWithQty;
            }
        }
        return foundProductWithQty;
    }

    /**
     * Checks if the product with a name productName exist on the list of Products from AllProducts class.
     * It is only one product with a name productName on the list (productName unique).
     * @param productName - name of the product
     * @return true if exist, false if does not exist
     */
    public boolean productExist(String productName) {
        return getProductByName(productName) != null;
    }

    /**
     * Checks if the product with a name productName combined with quantity exist on the list of Products from the cart.
     * It is only one product with a name productName on the list (productName unique).
     * @param productName - name of the product
     * @return true if exist, false if does not exist
     */
    public boolean productWithQtyExist(String productName) {
        return getProductWithQtyByName(productName) != null;
    }

    /**
     * Provides to add the product with a name productName combined with quantity to the list of Products from the cart.
     * If the product with a name productName is already on the cart, the product quantity is increasing by quantity
     * from parameter.
     * If the product with a name productName is not on the cart, the product is added to the cart with a quantity from
     * parameter.
     * In addition to adding the product to the cart, total amount of the cart and total quantity of products
     * are increasing.
     * @param productName - name of the product
     * @param quantity - value of the product quantity
     */
    public void addProductToCart(String productName, int quantity) {
        Product product = getProductByName(productName);
        if (productWithQtyExist(productName)) {
            ProductWithQty productWithQty = getProductWithQtyByName(productName);
            int productQty = productWithQty.getQuantity();
            productWithQty.setQuantity(productQty + quantity);
        } else {
            ProductWithQty productWithQty = new ProductWithQty(product, quantity);
            listOfProductsWithQty.add(productWithQty);
        }
        totalAmount = totalAmount.add(product.getPrice().multiply(BigDecimal.valueOf(quantity)));
        quantityOfProducts += quantity;
    }

    /**
     * Provides to remove the product with a name productName combined with quantity from the list of Products
     * from the cart.
     * If the product with a name of productName is found on the list of Products from the cart, it will be removed.
     * In addition to removing the product to the cart, total amount of the cart and total quantity of products
     * are decreasing.
     * @param productName - name of the product
     */
    public void removeProductFromCart(String productName) {
        ProductWithQty productWithQty = getProductWithQtyByName(productName);
        totalAmount = totalAmount.subtract(productWithQty.getProduct().getPrice()
                .multiply(BigDecimal.valueOf(productWithQty.getQuantity())));
        quantityOfProducts -= productWithQty.getQuantity();
        listOfProductsWithQty.remove(productWithQty);
    }
}
