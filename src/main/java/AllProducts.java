import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dzirafa on 19.02.2018
 */
public class AllProducts {
    private List<Product> allAvailableProducts = new ArrayList<>();

    public AllProducts() {
        Product product1 = new Product("Snickers", "Snickers 50g", BigDecimal.valueOf(1.99d));
        Product product2 = new Product("Mars", "Mars 50g", BigDecimal.valueOf(1.89d));
        Product product3 = new Product("Bounty", "Bounty 40g", BigDecimal.valueOf(1.49d));
        Product product4 = new Product("Twix", "Twix 60g", BigDecimal.valueOf(2.29d));
        Product product5 = new Product("Milky Way", "Milky Way 30g", BigDecimal.valueOf(1.29d));
        Product product6 = new Product("M&M", "M&M 100g", BigDecimal.valueOf(3.49d));

        allAvailableProducts.add(product1);
        allAvailableProducts.add(product2);
        allAvailableProducts.add(product3);
        allAvailableProducts.add(product4);
        allAvailableProducts.add(product5);
        allAvailableProducts.add(product6);
    }

    public List<Product> getAllAvailableProducts() {
        return allAvailableProducts;
    }
}
