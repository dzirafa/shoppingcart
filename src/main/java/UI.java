import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/**
 * Created by dzirafa on 19.02.2018
 */
public class UI {

    Cart cart = new Cart();
    private int choice = 0;

    public UI() {
        menu();
    }

    public void startMenu() {
        System.out.println("1. Display all available products");
        System.out.println("2. Display your cart");
        System.out.println("0. Exit");
    }

    public void productsMenu() {
        System.out.println("1. Add product to your cart");
        System.out.println("2. Remove product from your cart");
        System.out.println("3. Check if the product is in your cart");
        System.out.println("0. Exit");
    }

    public void menu() {
        do {
            startMenu();
            getUserChoice();

            switch (choice) {
                case 1:
                    displayAllProducts();
                    productsMenu();
                    getUserChoice();
                    menuAfter1();
                    break;
                case 2:
                    displayProductsFromCart();
                    break;
                case 0:
                    System.exit(0);
                    break;
                default:
                    break;
            }
        } while (choice != 0);
    }

    public void menuAfter1() {
        switch (choice) {
            case 1:
                addProduct();
                displayProductsFromCart();
                break;
            case 2:
                removeProduct();
                displayProductsFromCart();
                break;
            case 3:
                checkProductInCart();
                break;
            default:
                break;
        }
    }

    /**
     * Gets the the option number chosen by the user.
     * @exception InputMismatchException if user choose not a number (int)
     * @return the option number
     */
    public int getUserChoice() {
        Scanner scanner = new Scanner(System.in);
        try {
            choice = scanner.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Invalid value! Choose available option!");
            getUserChoice();
        }
        return choice;
    }

    /**
     * Gets the product name chosen by the user.
     * @return the product name
     */
    public String getUserString() {
        System.out.println("Choose the product name:");
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    /**
     * Displays all products from the list of products from AllProducts class
     */
    public void displayAllProducts() {
        List<Product> listOfProducts = new AllProducts().getAllAvailableProducts();
        for (Product product : listOfProducts) {
            System.out.println(product.toString());
        }
    }

    /**
     * Displays the product fields with it's quantity if the product exist on the list of products from the cart.
     * If the product does not exist on the list of products from the cart, it will be displayed information about it.
     */
    public void checkProductInCart() {
        String productName = getUserString();
        if (cart.productWithQtyExist(productName)) {
            ProductWithQty productWithQty = cart.getProductWithQtyByName(productName);
            System.out.println(productWithQty.getProduct().toString() + " --- quantity: "
                    + productWithQty.getQuantity());
        } else {
            System.out.println("The product is not in your cart.");
        }
    }

    /**
     * Displays all products with their fields and quantities from the list of products from the cart, total amount
     * of the cart and information about how many items how many products are on the list from the cart.
     * If there is no product on the list from the cart, it will be displayed information about it.
     */
    public void displayProductsFromCart() {
        if (cart.getListOfProductsWithQty().size() == 0) {
            System.out.println("The cart is empty.");
        } else {
            System.out.println("Your cart contains:");
            for (int i = 0; i < cart.getListOfProductsWithQty().size(); i++) {
                System.out.println(cart.getListOfProductsWithQty().get(i).getProduct().toString()
                        + " --- quantity: " + cart.getListOfProductsWithQty().get(i).getQuantity());
            }
        }
        System.out.println("Total amount: " + cart.getTotalAmount() + " (" + cart.getQuantityOfProducts() + " items of "
                + cart.getListOfProductsWithQty().size() + " products)");
    }

    /**
     * Adds the product with a name chosen by the user in quantity chosen by the user to the list of products from
     * the cart.
     * If the product with a name chosen by the user does not exist on the list of products from AllProducts class, it
     * will be displayed information about it.
     */
    public void addProduct() {
        String productName = getUserString();
        if (cart.productExist(productName)) {
            Product product = cart.getProductByName(productName);
            System.out.println("Choose quantity of the product:");
            int quantity = getUserChoice();
            cart.addProductToCart(product.getName(), quantity);
        } else {
            System.out.println("The product does not exist.");
        }
    }

    /**
     * Removes the product with a name chosen by the user with quantity from the list of products from the cart.
     * If the product with a name chosen by the user does not exist on the list of products from the cart, it
     * will be displayed information about it.
     */
    public void removeProduct() {
        String productName = getUserString();
        if (cart.productWithQtyExist(productName)) {
            cart.removeProductFromCart(productName);
        } else {
            System.out.println("The product does not exist in your cart.");
        }
    }


}
