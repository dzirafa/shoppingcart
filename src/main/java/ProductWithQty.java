/**
 * Created by dzirafa on 25.02.2018
 */
public class ProductWithQty {
    private Product product;
    private int quantity;

    public ProductWithQty(Product product, int quantity) {
        this.product = product;
        this.quantity = quantity;
    }

    public Product getProduct() {
        return product;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
