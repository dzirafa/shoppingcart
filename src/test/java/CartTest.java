import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by dzirafa on 26.02.2018
 */

public class CartTest {
    private Cart cart;

    @Before
    public void start() {
        cart = new Cart();
    }

    @Test
    public void shouldFindTheProductFromAllProducts() {
        Assert.assertEquals("Mars", cart.getProductByName("Mars").getName());
        Assert.assertEquals("Mars 50g", cart.getProductByName("Mars").getDescription());
        Assert.assertEquals(1.89d,
                cart.getProductByName("Mars").getPrice().doubleValue(), 0.0);
    }

    @Test
    public void cantFindTheProductFromAllProducts() {
        Assert.assertEquals(null, cart.getProductByName("Milka"));
    }

    @Test
    public void cantFindAnyProductInCart() {
        Assert.assertEquals(null, cart.getProductWithQtyByName("Milka"));
    }

    @Test
    public void canFindTheProductInCart() {
        cart.addProductToCart("Mars", 2);
        Assert.assertEquals("Mars", cart.getProductWithQtyByName("Mars").getProduct().getName());
        Assert.assertEquals("Mars 50g",
                cart.getProductWithQtyByName("Mars").getProduct().getDescription());
        Assert.assertEquals(1.89d,
                cart.getProductWithQtyByName("Mars").getProduct().getPrice().doubleValue(), 0.0);
        Assert.assertEquals(2, cart.getProductWithQtyByName("Mars").getQuantity());
    }

    @Test
    public void productExistInAllProducts() {
        Assert.assertTrue(cart.productExist("Mars"));
    }

    @Test
    public void productDoesNotExistInAllProducts() {
        Assert.assertTrue(!cart.productExist("Milka"));
    }

    @Test
    public void productExistInTheCart() {
        cart.addProductToCart("Mars", 10);
        Assert.assertTrue(cart.productWithQtyExist("Mars"));
    }

    @Test
    public void productDoesNotExistInTheCart() {
        Assert.assertTrue(!cart.productWithQtyExist("Milka"));
    }

    @Test
    public void oneProductShouldBeAddedToTheCart() {
        cart.addProductToCart("Mars", 10);
        Assert.assertEquals(1, cart.getListOfProductsWithQty().size());
        Assert.assertEquals(18.90d, cart.getTotalAmount().doubleValue(), 0.0);
        Assert.assertEquals(10, cart.getQuantityOfProducts());
    }

    @Test
    public void twoDifferentProductsShouldBeAddedToTheCart() {
        cart.addProductToCart("Mars", 10);
        cart.addProductToCart("Snickers", 10);
        Assert.assertEquals(2, cart.getListOfProductsWithQty().size());
        Assert.assertEquals(38.8d, cart.getTotalAmount().doubleValue(), 0.0);
        Assert.assertEquals(20, cart.getQuantityOfProducts());
    }

    @Test
    public void oneProductAddedTwoTimesToTheCart() {
        cart.addProductToCart("Mars", 5);
        cart.addProductToCart("Mars", 5);
        Assert.assertEquals(1, cart.getListOfProductsWithQty().size());
        Assert.assertEquals(18.90d, cart.getTotalAmount().doubleValue(), 0.0);
        Assert.assertEquals(10, cart.getQuantityOfProducts());
    }

    @Test
    public void productShouldBeRemovedFromTheCart() {
        cart.addProductToCart("Mars", 2);
        cart.removeProductFromCart("Mars");
        Assert.assertEquals(0, cart.getListOfProductsWithQty().size());
        Assert.assertEquals(0d, cart.getTotalAmount().doubleValue(), 0.0);
        Assert.assertEquals(0, cart.getQuantityOfProducts());

    }
}
