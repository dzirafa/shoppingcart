import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;

/**
 * Created by dzirafa on 28.02.2018
 */
@RunWith(Parameterized.class)
public class ProductTest {

    private Product firstProduct;

    @Parameterized.Parameter(value = 0)
    public Product secondProduct;

    @Parameterized.Parameter(value = 1)
    public boolean equalsResult;

    @Parameterized.Parameters
    public static Collection<Object[]> dataProvider() {
        return Arrays.asList(new Object[][]{
                {new Product("Ritter Sport", "Ritter Sport 100g", BigDecimal.valueOf(4.99)), true},
                {new Product("Ritter", "Ritter Sport 100g", BigDecimal.valueOf(4.99)), false},
                {new Product("Ritter Sport", "Ritter 100g", BigDecimal.valueOf(4.99)), false},
                {new Product("Ritter Sport", "Ritter Sport 100g", BigDecimal.valueOf(0.99)), false},
                {new Product(), false}
        });
    }

    @Before
    public void start() {
        firstProduct = new Product("Ritter Sport", "Ritter Sport 100g",
                BigDecimal.valueOf(4.99));
    }


    @Test
    public void equalsTrueHashCodeTheSame() {
        if (firstProduct.equals(secondProduct)) {
            Assert.assertEquals(firstProduct.hashCode(), secondProduct.hashCode());
        }
    }

    @Test
    public void equalsFalseHashCodeDifferent() {
        if (firstProduct.hashCode() != secondProduct.hashCode()) {
            Assert.assertTrue(!firstProduct.equals(secondProduct));
            Assert.assertTrue(!secondProduct.equals(firstProduct));
        }
    }

    @Test
    public void equalsTrue() {
        Assert.assertEquals(firstProduct.equals(secondProduct), equalsResult);
        Assert.assertEquals(secondProduct.equals(firstProduct), equalsResult);
    }
}
