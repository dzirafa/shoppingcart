# Shopping Cart

This is a simple application to manage shopping cart. There is no GUI. It's made for recruitment purpose by Rafal Dzido.

## Functionality

The app allows to create user's shopping cart. It displays all available products from which the user can add to the cart and define its quantity. The cart stores and can display the list of products chosen by the user, total amount of the cart, total number of items and total number of products. The user can check if the product is in the cart and if it is, the product can be removed from the cart.

## Assumptions

* The name of the product (productName) is unique.
* In order to check functionality of the app, it is prepared AllProducts class with predefined products in the constructor.
* It is possible to add product to the cart only from the list from AllProducts class.
* The product can be added to the list only once - re-adding the same product should only increase the amount of this product in the cart

## Getting Started

### Installing

In order to start the application on your local machine it's needed to load pom.xml file (with all dependencies) on your IDE.

## Built With

* Java SE 8
* Maven – dependency management

## Ideas for developement

* Adding possibility to keep data in database
* Adding GUI

## Author

* **Rafal Dzido** - www.linkedin.com/in/rafał-dzido/